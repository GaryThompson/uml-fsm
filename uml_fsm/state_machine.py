# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This module provides FSM primitives
"""
import abc
import asyncio
import logging
import re
import time
from typing import Optional

logger = logging.getLogger(__name__)


class StateMachine(abc.ABC):
    """
    A state machine class / interface complete with a simple event system.

    The aim of this state machine is to produce a declarative interface
    that uses events to trigger transitions from the active state.

    The base class

    To declare events create event class constants of the form "EVENT_[A-Z_]*".
    The value of these constants is a string in the "[a-z_]*" form which is
    used in the state classes.
    """

    PLANTUML = ""
    UML_DIAGRAM = ""

    _TOKEN_TERMINATOR = "[*]"
    _STATE_RE = re.compile(r"^(?P<name>(\[\*]|\w+))( : (?P<comment>[\w ]+))?$")
    _STATE_PROPERTY_RE = re.compile(
        r"^(?P<name>(\[\*]|\w+)) : (?P<property>[\w ]+) *= *(?P<value>[\w ]+)$"
    )
    _TRANSITION_RE = re.compile(
        r"^(?P<from_state>((\[\*])|\w+)) *--> *(?P<to_state>(\[\*]|\w+))( : (?P<name>\w+))?$"
    )
    # https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case#1176023
    _CAMEL_CASE_RE = re.compile(r"(?<!^)(?=[A-Z])")

    def __init__(self):
        self.states = {}
        self.properties = {}
        self.transitions = {}
        self.first_state = ""
        self.last_states = set()
        self.state = ""
        self.time_in_state = 0
        self.error = None
        self._state_task: Optional[asyncio.Task] = None
        self._timeout_task: Optional[asyncio.Task] = None
        self.input_event = asyncio.Event()
        self._parse_uml()

    @property
    def completed_ok(self) -> bool:
        return not self.error

    @staticmethod
    def _map_state_name(name: str) -> str:
        return StateMachine._CAMEL_CASE_RE.sub("_", name).lower()

    _STATE_PROPERTY_CLEAN_FX = {
        "timeout": float,
        "timeout_state": _map_state_name,
        "error_state": _map_state_name,
    }

    def _parse_uml(self):
        for line in self.PLANTUML.split("\n"):
            line = line.strip()
            if self._STATE_RE.match(line):
                m = self._STATE_RE.match(line).groupdict()
                state_name = self._map_state_name(m["name"])
                self.states[state_name] = m["comment"]
                logger.debug("Init:  Found state %s: %s", m["name"], m["comment"])
            elif self._STATE_PROPERTY_RE.match(line):
                m = self._STATE_PROPERTY_RE.match(line).groupdict()
                state_name = self._map_state_name(m["name"])
                p_name = m["property"]
                p_value = m["value"]
                if p_name in self._STATE_PROPERTY_CLEAN_FX:
                    p_value = self._STATE_PROPERTY_CLEAN_FX[p_name](p_value)
                p_dict = self.properties.setdefault(state_name, {})
                p_dict[p_name] = p_value
                logger.debug(
                    "Init:  Found state property %s: %s=%s",
                    m["name"],
                    m["property"],
                    m["value"],
                )
            elif self._TRANSITION_RE.match(line):
                m = self._TRANSITION_RE.match(line).groupdict()
                if m["from_state"] == self._TOKEN_TERMINATOR:
                    assert not self.first_state, "Only one first state can be declared"
                    self.first_state = self._map_state_name(m["to_state"])
                    logger.debug("Init:  Found First State %s", self.first_state)
                    if self.first_state not in self.states:
                        self.states[self.first_state] = ""
                elif m["to_state"] == self._TOKEN_TERMINATOR:
                    s = self._map_state_name(m["from_state"])
                    self.last_states.add(s)
                    logger.debug("Init:  Found Last State %s", s)
                    if s not in self.states:
                        self.states[s] = ""
                else:
                    from_state = self._map_state_name(m["from_state"])
                    to_state = self._map_state_name(m["to_state"])
                    from_state_transitions = self.transitions.setdefault(from_state, {})
                    from_state_transitions[m["name"]] = to_state
                    logger.debug(
                        "Init:  Found Transition %s: %s -> %s",
                        m["name"],
                        m["from_state"],
                        m["to_state"],
                    )
                    if from_state not in self.states:
                        self.states[from_state] = ""
                    if to_state not in self.states:
                        self.states[to_state] = ""
            else:
                logger.debug("Skipping line: %s", line)

    def get_enter_state_function(self, state_name: str):
        return getattr(self, f"on_enter_{state_name}", None)

    def get_timeout_state_function(self, state_name: str):
        return getattr(self, f"on_timeout_{state_name}", None)

    def get_test_transition_function(self, transition_name: str):
        return getattr(self, f"test_{transition_name}", None)

    def get_exit_state_function(self, state_name: str):
        return getattr(self, f"on_exit_{state_name}", None)

    def get_state_timeout(self) -> Optional[int]:
        if self.state in self.properties:
            return self.properties[self.state].get("timeout")
        else:
            return None

    def get_state_timeout_state(self) -> Optional[str]:
        """
        When a timeout occurs this is the state to move to
        """
        if self.state in self.properties:
            return self.properties[self.state].get("timeout_state")
        else:
            return None

    def get_state_error_state(self) -> Optional[str]:
        if self.state in self.properties:
            return self.properties[self.state].get("error_state")
        else:
            return None

    def notify_input(self):
        """
        To avoid constant polling of transitions, the StateMachine
        will pause and wait until an event has occurred that will have
        changed the input space.  Child classes can call this function
        to trigger the StateMachine to continue processing state.
        """
        self.input_event.set()

    def _get_state_transitions(self) -> dict:
        if self.state in self.transitions:
            return self.transitions[self.state]
        else:
            return {}

    def _get_state_transition_functions(self) -> list:
        return [
            (self.get_test_transition_function(fx_name), _to_state)
            for fx_name, _to_state in self._get_state_transitions().items()
            if self.get_test_transition_function(fx_name)
        ]

    def _get_next_transition(self) -> Optional[str]:
        tx = self._get_state_transitions()
        if tx:
            return next(iter(tx.values()))
        else:
            return None

    async def _wait_for_state_change_event(self):
        await self.input_event.wait()
        self.input_event.clear()

    async def _exec_step_and_transition(self) -> str:
        """
        This executes a complete step and transition check.  Returns
        the next step based on successful transition.

        The process is:
        * Execute the current enter state function
        * Loop forever until a new_state is determined based on the
          transitions available.  However, on each loop if no transitions
          pass the loop pauses until notify_input is called.  This behaviour
          can be overridden by changing the function _wait_for_state_change_event
        * Execute the exit state function

        """
        try:
            self.time_in_state = time.time()
            enter_state_fx = self.get_enter_state_function(self.state)
            if enter_state_fx:
                await enter_state_fx()

            # Scan transitions
            transition_functions = self._get_state_transition_functions()
            if transition_functions:
                new_state = None
                while new_state is None:
                    for fx_name, to_state in transition_functions:
                        if await fx_name():
                            new_state = to_state
                            break
                    if new_state is None:
                        await self._wait_for_state_change_event()
            else:
                new_state = self._get_next_transition()

            # Exit State
            exit_state_fx = self.get_exit_state_function(self.state)
            if exit_state_fx:
                await exit_state_fx()

        except Exception as e:
            self.error = e
            error_state = self.get_state_error_state()
            if error_state:
                new_state = error_state
            else:
                logger.error(
                    "An unexpected exception occurred executing states or transitions: %s",
                    e,
                    exc_info=e,
                )
                raise e
        finally:
            if self._timeout_task:
                self._timeout_task.cancel()

        return new_state

    async def _timeout_function(self, timeout: int):
        """
        Will cancel the given task after a set amount of time
        """
        await asyncio.sleep(timeout)
        if self._state_task:
            self._state_task.cancel()

    async def run_to_completion(self):
        """
        Starts the state machine running from the first step and will
        exit when the state machine reaches the final step.

        Since the UMl is intended to be the declaration for behaviour,
        this function looks for async methods that map each transition name
        and if that function returns true, then the transition is executed.

        If there are multiple transition functions and one is declared,
        then all need to be declared.
        """
        self.state = self.first_state
        if not self.last_states.issubset(set(self.states.keys())):
            raise ValueError(f"A last state is required and not provided")

        while True:
            self._state_task = asyncio.create_task(self._exec_step_and_transition())
            timeout_time = self.get_state_timeout()
            if timeout_time is not None:
                self._timeout_task = asyncio.create_task(
                    self._timeout_function(timeout_time)
                )
            else:
                self._timeout_task = None

            # Collect tasks
            await asyncio.gather(
                *[x for x in (self._state_task, self._timeout_task) if x],
                return_exceptions=True,
            )

            if self._state_task.cancelled():
                new_state = self.get_state_timeout_state()
            elif self._state_task.exception():
                # An exception occurred, exit the loop
                break
            else:
                new_state = self._state_task.result()

            self._state_task = None
            self._timeout_task = None

            if new_state is None:
                if self.state not in self.last_states:
                    logger.warning(
                        "StateMachine does not provide a transition for state %s",
                        self.state,
                    )
                break
            else:
                logger.debug("Transition %s --> %s", self.state, new_state)
                self.state = new_state
