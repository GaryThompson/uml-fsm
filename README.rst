=======
UML FSM
=======

Introduction
============

This is a Finite State Machine designed to be declared using a PlantUML
diagram.

How it works
============

When looking around for a simple FSM package in python I did not find
anything that was light, simple and mature.  So I decided to create
my own.  One of the things I like about using a FSM is the opportunity
for having a nice diagram to illustrate it's operations.  From there,
I wondered if I could create a FSM that uses PlantUML as the basis
for its declaration combined with some simple conventions for the
implementation.

For state diagrams, see here:  https://plantuml.com/state-diagram

To create a diagram, see here:  https://www.plantuml.com/plantuml/uml

State Machine Workflow
======================

First, create a PlantUML State Diagram and assign this to a StateMachine class.

All states go through the following process:

* async def on_enter_<state> - This is an event raised when a state is entered, it should
  execute any output actions and terminate with minial blocking
* async def on_exit_<state> - This is called after an input event to indicate a state exit
* async def <transition> - The state machine will call each transition function for a state
  (if it is declared) and if the function returns true then execute the transition.  These
  functions are declared as part of the UML.

See the manual_run.py file in the tests folder for a simple example
