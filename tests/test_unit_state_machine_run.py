# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
"""
These tests are as isolated as reasonable in order to test run performance
"""
import asyncio

from uml_fsm.state_machine import StateMachine


def test_enter_first_state():
    flag = False

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            [*] --> State1 : first
            State1 --> State2 : second
            State2 --> [*]
            @enduml
            """

        # noinspection PyMethodMayBeStatic
        async def on_enter_state1(self):
            nonlocal flag
            flag = True

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())

    assert flag
    assert t.completed_ok


def test_enter_last_state():
    flag = False

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            [*] --> State1 : first
            State1 --> State2 : second
            State2 --> [*]
            @enduml
            """

        # noinspection PyMethodMayBeStatic
        async def on_enter_state2(self):
            nonlocal flag
            flag = True

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())

    assert flag
    assert t.completed_ok


def test_run_to_completion_timeout():
    """
    This test implements a timeout on a step.
    """
    flag = False

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            State1 : timeout=1
            State1 : timeout_state=State3
            [*] --> State1 : first
            State1 --> State2 : second
            State2 --> [*]
            State3 --> [*]
            @enduml
            """

        # noinspection PyMethodMayBeStatic
        async def on_enter_state1(self):
            await asyncio.sleep(5)

        # noinspection PyMethodMayBeStatic
        async def on_enter_state3(self):
            nonlocal flag
            flag = True

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())

    assert flag
    assert t.completed_ok


def test_run_to_completion_state_exception_no_error_handling():
    """
    When an exception occurs in a state and no error handling is
    specified,
    """

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            [*] --> State1 : first
            State1 --> [*]
            @enduml
            """

        # noinspection PyMethodMayBeStatic
        async def on_enter_state1(self):
            raise Exception("This is not handled")

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())

    assert not t.completed_ok
    assert {"state1"} == t.last_states


def test_run_to_completion_state_exception_jump_state():
    """
    When an exception occurs in a state, the error is stored and an
    error state can be specified.
    """
    flag = None
    err = Exception("This is handled")

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            State1 : error_state=StateE
            [*] --> State1 : first
            State1 --> [*]
            StateE --> [*]
            @enduml
            """

        # noinspection PyMethodMayBeStatic
        async def on_enter_state1(self):
            raise err

        # noinspection PyMethodMayBeStatic
        async def on_enter_state_e(self):
            nonlocal flag
            flag = err

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())

    assert err == flag
    assert err == t.error
    assert not t.completed_ok
