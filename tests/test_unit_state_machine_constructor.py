# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
"""
These are not strictly unit tests (from an isolation perspective), but
are very simplified.
"""

from uml_fsm.state_machine import StateMachine


def test_parse_state():
    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            Init : The initial state
            @enduml
            """

    t = TestStateMachine()
    assert {"init": "The initial state"} == t.states
    assert "" == t.first_state


def test_parse_transition():
    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            State2 : A state 2
            State1 --> State2 : transition_state 
            @enduml
            """

    t = TestStateMachine()
    assert {"state1": "A state 1", "state2": "A state 2"} == t.states
    assert {"state1": {"transition_state": "state2"}} == t.transitions
    assert "" == t.first_state


def test_parse_first_state():
    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            [*] --> State1 
            @enduml
            """

    t = TestStateMachine()
    assert {"state1": "A state 1"} == t.states
    assert {} == t.transitions
    assert "state1" == t.first_state


def test_parse_last_state():
    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            State1 : A state 1
            State1 --> [*] 
            @enduml
            """

    t = TestStateMachine()
    assert {"state1": "A state 1"} == t.states
    assert {} == t.transitions
    assert "" == t.first_state
    assert {"state1"} == t.last_states
