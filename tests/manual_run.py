# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
import random
import asyncio
import logging

from uml_fsm.state_machine import StateMachine

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    class TestStateMachine(StateMachine):
        # noinspection SpellCheckingInspection
        PLANTUML = """
            @startuml
            Init : The initial state
            FirstState : The first state encountered
            SecondState :  This will start a timer
            WaitForTimer : Waiting for input

            [*] --> Init
            Init --> FirstState : start
            FirstState --> SecondState : first_step
            SecondState --> WaitForTimer : second_step
            WaitForTimer --> SecondState : timer_finished_random_loop
            WaitForTimer --> Done : timer_finished
            Done --> [*]
            @enduml
            """

        UML_DIAGRAM = "//www.plantuml.com/plantuml/png/RP31IWGn38RlUOgSXNq13x87MV2y2nv4XhBDk85EigGPV7sRZe9rl2NvluIlFLPFvkjIu4NOyH4lDq9k9QU2BNE24rlrSvJVyHo3BKIIgwxYP9JXJ5UL_4Csb2j-Skbtp36Xyq86hudzf7Q9fd7HidpWh8OizzK1uEtX7G-79modJIowGQJV_EqMn6ZGFQVgT1_K0lpZK7kyeKEqF-endsOMhZVAaoN9kan5zP_DPvNz2lHfnEsLS2J9xVU_0000"  # noqa

        def __init__(self):
            super().__init__()
            self.timing = False

        async def _timer(self):
            await asyncio.sleep(3)
            logger.debug("Ending timer")
            self.timing = False
            self.notify_input()

        def start_timer(self):
            self.timing = True
            asyncio.create_task(self._timer())

        # noinspection PyMethodMayBeStatic
        async def on_enter_init(self):
            logger.debug("Entered Init")

        # noinspection PyMethodMayBeStatic
        async def on_enter_second_state(self):
            logger.debug("Entered Second State")
            self.start_timer()

        # noinspection PyMethodMayBeStatic
        async def timer_finished_random_loop(self):
            return not self.timing and random.random() < 0.5

        # noinspection PyMethodMayBeStatic
        async def timer_finished(self):
            return not self.timing

    t = TestStateMachine()
    asyncio.run(t.run_to_completion())
